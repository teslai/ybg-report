package com.ybg.report.rtemplate.service;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import com.ybg.report.rtemplate.dao.RtemplateDao;

import com.ybg.report.rtemplate.domain.RtemplateVO;
import com.ybg.report.rtemplate.qvo.RtemplateQuery;
import java.util.List;

import com.ybg.base.jdbc.BaseMap;
import com.ybg.base.util.Page;

/**
 * 
 * 
 * @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-21
 */

@Repository
public class RtemplateServiceImpl implements RtemplateService {
	@Autowired
	private RtemplateDao rtemplateDao;
	
	@Override	
	public RtemplateVO save(RtemplateVO bean) throws Exception{
	return rtemplateDao.save(bean);
	
	}
	
	@Override
	public	void update(BaseMap<String, Object> updatemap, BaseMap<String, Object> wheremap){
		rtemplateDao.update(updatemap,wheremap);
	}
	
	
	@Override
	public	Page list(Page page,RtemplateQuery qvo)throws Exception{
		return rtemplateDao.list(page,qvo);
	}
	
	@Override
	public	List<RtemplateVO> list(RtemplateQuery qvo) throws Exception{
		return rtemplateDao.list(qvo);
	}
   @Override
	public void remove(BaseMap<String, Object> wheremap){
		rtemplateDao.remove(wheremap);
	}
   @Override
	public RtemplateVO get(String id){
		return 	rtemplateDao.get(id);
	}
}
